<?php

declare(strict_types=1);

namespace UtilityKit\Utility;

use UtilityKit\Utility\System\SystemFactorUtility;

class ProcessIdUtility
{
    protected SystemFactorUtility $systemFactorUtility;

    public function __construct(SystemFactorUtility $systemFactorUtility)
    {
        $this->systemFactorUtility = $systemFactorUtility;
    }

    /**
     * @return string
     */
    public function generate():string
    {
        $uniqueId = $this->getUniqueId();
        return sprintf('%s-%s-%s-%s-%s', $this->getDate(), $this->getTime(), $this->systemFactorUtility->getMicroSecond(), substr($uniqueId, 0, 4), substr($uniqueId, 4, 8));
    }

    /**
     * @return string
     */
    protected function getDate():string
    {
        return date("Ymd", $this->systemFactorUtility->getTimestamp());
    }

    /**
     * @return string
     */
    protected function getTime():string
    {
        return date('His', $this->systemFactorUtility->getTimestamp());
    }

    /**
     * @return string
     */
    protected function getUniqueId(): string
    {
        return $this->systemFactorUtility->getUniqueId();
    }
}