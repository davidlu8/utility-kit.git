<?php

declare(strict_types=1);

namespace UtilityKit\Utility\System;

interface SystemFactorUtility
{
    /**
     * 获取当前时间戳
     * @return int
     */
    public function getTimestamp(): int;

    /**
     * 获取当前时间微秒
     * @return string
     */
    public function getMicroSecond(): string;

    /**
     * 获取唯一键ID
     * @return string
     */
    public function getUniqueId(): string;
}