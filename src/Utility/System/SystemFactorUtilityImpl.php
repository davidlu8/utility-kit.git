<?php

declare(strict_types=1);

namespace UtilityKit\Utility\System;

class SystemFactorUtilityImpl implements SystemFactorUtility
{
    public function getTimestamp(): int
    {
        return time();
    }

    public function getMicroSecond(): string
    {
        $data = explode(" ", microtime());
        return substr($data[0], 2, 6);
    }

    public function getMicroTime(): float
    {
        return microtime(true);
    }

    public function getUniqueId(): string
    {
        return md5(uniqid((string)mt_rand(), true));
    }
}