<?php

declare(strict_types=1);

namespace UtilityKit\Tests\Utility;

use PHPUnit\Framework\MockObject\MockObject;
use UtilityKit\Tests\TestCase;
use UtilityKit\Utility\ProcessIdUtility;
use UtilityKit\Utility\System\SystemFactorUtility;

class ProcessIdUtilityTest extends TestCase
{
    /**
     * @return array
     */
    public function dpGenerateSceneGenerateAssertTrue(): array
    {
        return [
            [
                1595921623,
                960436,
                '8aa78a0cafca683b8269352141c4cf4b',
                '20200728-073343-960436-8aa7-8a0cafca'
            ],
            [
                1595922497,
                231758,
                'fbe9eb6bb44f9d4cc4894a7bdf639d3b',
                '20200728-074817-231758-fbe9-eb6bb44f'
            ],
            [
                1595922710,
                995906,
                '33cefe5471c1ba723248873130e6ffa1',
                '20200728-075150-995906-33ce-fe5471c1'
            ]
        ];
    }

    /**
     * @dataProvider dpGenerateSceneGenerateAssertTrue
     * @param $timestamp
     * @param $microSecond
     * @param $uniqueId
     * @param $excepted
     */
    public function testGenerateSceneGenerateAssertTrue($timestamp, $microSecond, $uniqueId, $excepted)
    {
        /** @var MockObject|SystemFactorUtility $systemFactorStub */
        $systemFactorStub = $this->getMockBuilder(SystemFactorUtility::class)
            ->setMethods(['getTimestamp', 'getMicroSecond', 'getUniqueId'])
            ->disableOriginalConstructor()
            ->getMock();
        $systemFactorStub->method('getTimestamp')
            ->willReturn($timestamp);
        $systemFactorStub->method('getMicroSecond')
            ->willReturn($microSecond);
        $systemFactorStub->method('getUniqueId')
            ->willReturn($uniqueId);
        $processIdUtility = new ProcessIdUtility($systemFactorStub);
        $this->assertEquals($excepted, $processIdUtility->generate());
    }
}