<?php
declare(strict_types=1);

namespace UtilityKit\Tests\Utility;

use Contract\Exceptions\ValidationException;
use UtilityKit\Tests\TestCase;
use UtilityKit\Utility\DateTimeUtility;

class DateTimeUtilityTest extends TestCase
{
    public function dpGetMomDateInterval(): array
    {
        return [
            [
                '2022-04-04',
                '2022-04-10',
                'Y-m-d',
                [
                    '2022-03-28', '2022-04-03'
                ]
            ],
            [
                '2022-04-01',
                '2022-04-10',
                'Y-m-d',
                [
                    '2022-03-22', '2022-03-31'
                ]
            ],

            [
                '2022-02-01',
                '2022-02-28',
                'Y-m-d',
                [
                    '2022-01-04', '2022-01-31'
                ]
            ]
        ];
    }

    /**
     * @dataProvider dpGetMomDateInterval
     * @param string $startTime
     * @param string $endTime
     * @param string $format
     * @param array $dateInterval
     * @throws ValidationException
     */
    public function testGetMomDateInterval(string $startTime, string $endTime, string $format, array $dateInterval)
    {
        $this->assertEquals($dateInterval, DateTimeUtility::getMomDateInterval($startTime, $endTime, $format));
    }

    public function dpGetYoyDateInterval(): array
    {
        return [
            [
                '2022-04-04',
                '2022-04-10',
                'Y-m-d',
                [
                    '2021-04-04', '2021-04-10'
                ]
            ],
            [
                '2022-04-01',
                '2022-04-10',
                'Y-m-d',
                [
                    '2021-04-01', '2021-04-10'
                ]
            ],

            [
                '2020-02-01',
                '2020-02-29',
                'Y-m-d',
                [
                    '2019-02-01', '2019-03-01'
                ]
            ]
        ];
    }

    /**
     * @dataProvider dpGetYoyDateInterval
     * @param string $startTime
     * @param string $endTime
     * @param string $format
     * @param array $dateInterval
     * @throws ValidationException
     */
    public function testGetYoyDateInterval(string $startTime, string $endTime, string $format, array $dateInterval)
    {
        $this->assertEquals($dateInterval, DateTimeUtility::getYoyDateInterval($startTime, $endTime, $format));
    }

    public function testMicroTimeFloat()
    {
        $this->assertEquals(1, DateTimeUtility::floatMicroTime());
    }

    public function testMilliSecond()
    {
        $this->assertEquals(1, DateTimeUtility::milliSecond());
    }

    public function testMicroSecond()
    {
        $this->assertEquals(1, DateTimeUtility::microSecond());
    }
}